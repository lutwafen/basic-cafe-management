# Uygulama Hakkında

- Adisyon bilgilerini tutabilirsiniz
- Her adisyona ait olan adisyon hareketlerini listeyebilirsiniz
- Ürün işlemlerinizi yapabilirsiniz
- Randevular oluşturabilir ve bu randevuları yönetebilirsiniz
- Ödeme listesini görebilirsiniz her ödemenin detayına, ödeme türüne göre işlemler yapabilirsiniz.
- Masalar oluşturabilirsiniz ve adisyonlarınız ile ilişkilendirebilirsiniz

> Ödev amacıyla yapılmıştır. Eksikleri veya hataları olabilir.
