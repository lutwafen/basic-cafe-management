<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Method: *");
class Product extends CI_Controller
{
    private $resp = null;

    public function __construct()
    {
        parent::__construct();
        $this->resp             = new stdClass();
        $this->resp->req_status = false;
    }

    function list($select_type = "ALL") {
        $select_type = strtoupper($select_type);
        $this->db->select("*");
        $this->db->from("vw_productlist");
        switch ($select_type) {
            case "FOOD":
                $this->db->where('Type', '0');
                break;
            case "DRINK":
                $this->db->where('Type', '1');
                break;
        }
        if (post('like_text')) {
            $this->db->like('Title', post('like_text'));
        }
        $this->db->order_by('ProductID', 'DESC');

        $result = $this->db->get()->result();

        if (count($result) > 0) {
            $this->resp->product_list = $result;
            $this->resp->req_status   = true;
        } else {
            $this->resp->error_message = "Böyle bir ürün yok veya hatalı bir işlem yaptınız";
        }
        api_result($this->resp);
    }

    public function CreateProduct()
    {
        // Control
        if (post('product_name')) {

            $this->db->select('ID');
            $this->db->from('product');
            $this->db->where('Title', post('product_name'));
            $result = $this->db->get()->result();

            if (count($result) > 0 && isset($result[0]->ID)) {
                $this->resp->error_message = "Var olan bir ürünü ekleyemezsiniz";
            } else {
                if (
                    "'" . post("sale_price") . "'"
                    &&
                    "'" . post("purchase_price") . "'"
                    &&
                    "'" . post("type") . "'"
                ) {
                    // Insert
                    reConnect();
                    $values = [
                        "product_title"          => post('product_name'),
                        "product_purchase_price" => post('purchase_price'),
                        "product_sale_price"     => post('sale_price'),
                        "product_type"           => "'".post('type')."'",
                    ];
                    $result = SelectProcedure('prcInsertProduct', $values);
                    if (count($result) > 0 && isset($result[0]->InsertedProductID)) {
                        $this->resp->req_status         = true;
                        $this->resp->product_id         = $result[0]->InsertedProductID;
                        // Ekstra olarak sunucuyu yormanın mantığı yok;
                        //eğer req_status değeri true ise; client tarafından gerekli işlemler halledilebilir. 😘
                        //____________________________________________________________________________________________

                        // reConnect();
                        // // Get product item
                        // $this->db->select("*");
                        // $this->db->from('vw_productlist');
                        // $this->db->where('ProductID', $this->resp->insert_product_id);
                        // $this->resp->product_item = $this->db->get()->result()[0];
                    }
                }
            }
        } else {
            $this->resp->error_message = "Hatalı veya Eksik Parametre Gönderdiniz!";
        }
        // Result
        api_result($this->resp);
    }

    public function EditProductItem()
    {
        // Ürün ID Kontrol
        if (post('product_id') > 0) {

            $product_item = $this->db
                ->select("*")
                ->from("vw_productlist")
                ->where('ProductID', post('product_id'))
                ->get()
                ->result();

            if (isset($product_item[0])) {
               $this->resp->req_status = true;
               // Post verileri Kontrol
               reConnect();
               if(
                    (
                        "'" . post("product_count") . "'"
                        &&
                        "'" . post("sale_price") . "'"
                        &&
                        "'" . post("purchase_price") . "'"
                        &&
                        "'" . post("type") . "'"
			            &&
                        post('product_title')
                    )
                    &&
                    ExecuteProcedure('prcUpdateProductItem', [
                        'pID' => post('product_id'), 
                        'title' => post('product_title'),
                        'type' => "'" . post("type") . "'",
                        'purchase' => post("purchase_price"),
                        'sale' => post("sale_price")
                    ])
               )
               {
                   $this->resp->req_status = true;
               }
               else{
                $this->resp->error_message = "Eksik veya Hatalı parametre gönderimi gerçekleşti!";     
			   }

            } else {
                $this->resp->error_message = "Böyle bir ürün yok veya ürün kodu hatalı!";
            }
        } else {
            $this->resp->error_message = "Eksik veya Hatalı parametre gönderimi gerçekleşti!";
        }

        api_result($this->resp);
    }


    public function RemoveProductItem(){
        // procedure: prcRemoveProductItem
        // params : [productID]
    
        if(post('product_id'))
        {
            if(ExecuteProcedure("prcRemoveProductItem", array('productID' => post('product_id'))))
            {
                $this->resp->req_status = true;
            }
            else{
               $this->resp->error_message = "Bir hata var sebebini bende bilmiyorum";     
			}
        }
        else{
            $this->resp->error_message = "Hatalı veya Eksik Parametre Gönderdiniz!";  
		}
  
        api_result($this->resp);

  }


}
