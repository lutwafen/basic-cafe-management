
<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Method: *");

class Desk extends CI_Controller {
	private $resp = null;
	function __construct() {
		parent::__construct();
		$this->load->model("Desk_Model", "Desk");
		$this->resp = new stdClass();
		$this->resp->req_status = false;
	}

	function List() {
		$likeText = post("desk_name") ? post('desk_name') : "";
		//print_r();
		echo json_encode($this->Desk->List($likeText), JSON_UNESCAPED_UNICODE);
	}

	function Create() {
		if (
			post('desk_title')
			&&
			"'".post('desk_position')."'"
		) {
			if ($this->db->insert('desk', array(
				'DeskTitle' => post('desk_title'),
				'DeskPosition' => post('desk_position'),
			))) {
				$this->resp->req_status = true;
                $desk_id = $this->db->insert_id();
                reConnect();
                $this->db->select('*');
                $this->db->from('vw_desklist');
                $this->db->where('ID', $desk_id);
                $this->resp->created_item = $this->db->get()->result()[0];
			} else {
				$this->resp->error_message = "Bir Sorun Var Daha Sonra Tekrar Deneyin!";
			}
		} else {
			$this->resp->error_message = "Hatalı Parametre Gönderimi!";
		}

		echo json_encode($this->resp, JSON_UNESCAPED_UNICODE);
	}

	function Delete() {
		if (
			post('desk_id')
			&&
			post('desk_id') > 0
		) {
			if ($this->db->delete('desk', array("ID" => post('desk_id')))) {
				$this->resp->req_status = true;
			} else {
				$this->resp->error_message = "Bir Sorun Var Daha Sonra Tekrar Deneyin!";
			}
		} else {
			$this->resp->error_message = "Hatalı Parametre Gönderimi!";
		}

		echo json_encode($this->resp, JSON_UNESCAPED_UNICODE);
	}

	function Update() {
		if (
			post('desk_id')
			&&
			post('desk_title')
			&&
			"'" . post('desk_position') . "'"
			&&
			"'" . post('desk_is_open') . "'"
		) {
			$updateArray = array(
				'DeskTitle' => post('desk_title'),
				'DeskPosition' => post('desk_position'),
				'DeskIsOpen' => post('desk_is_open'),
			);
			$this->db->where(array('ID' => post('desk_id')));
			if ($this->db->update('desk', $updateArray)) {
				$this->resp->req_status = true;
                reConnect();
                $this->db->select('*');
                $this->db->from('vw_desklist');
                $this->db->where('ID', post('desk_id'));                
		        $this->resp->updated_item = $this->db->get()->result()[0];
            } else {
				$this->resp->error_message = "Bir Sorun Var Daha Sonra Tekrar Deneyin!";
			}
		} else {
			$this->resp->error_message = "Hatalı Parametre Gönderimi!";
		}

		echo json_encode($this->resp, JSON_UNESCAPED_UNICODE);
	}
}