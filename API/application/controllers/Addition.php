<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Method: *");
class Addition extends CI_Controller
{
    private $resp = null;
    public function __construct()
    {
        parent::__construct();
        $this->resp             = new stdClass();
        $this->resp->req_status = false;
    }

    public function AdditionList()
    {
        $this->db->select("*");
        $this->db->from("vw_additionlist");
        $this->db->order_by('AdditionID', 'DESC');
        $query = $this->db->get()->result();

        if (count($query) > 0) {
            echo json_encode($query, JSON_UNESCAPED_UNICODE);
        } else {
            echo json_encode(
                array('req_status' => false,
                    'error_message'    => "Herhangi bir işlem geçmişi bulunamadı!"),
                JSON_UNESCAPED_UNICODE
            );
        }

    }

    public function CreateAddition()
    {
        if (post('desk_id')) {
            $this->db->select('*');
            $this->db->from("desk");
            $this->db->where('ID', post('desk_id'));
            //$result = (isset($this->db->get()->result()[0]) && count($this->db->get()->result()) > 0) ? $this->db->get()->result[0] : false;
            $result = $this->db->get()->result();
            if (
                count($result) > 0
                &&
                isset($result[0])
            ) {

                if (
                    $result[0]->DeskIsOpen == 1
                ) {
                    $this->resp->error_message = "Lütfen Kapalı Bir Masa Seçiniz!";
                } else {
                    reConnect();
                    $addition_id = SelectProcedure("prcCreateAddition", array('deskID' => post('desk_id')))[0]->AdditionID;

                    if ($addition_id > 0) {
                        $this->resp->addition_id = $addition_id;
                        $this->resp->req_status  = true;
                    } else {
                        $this->resp->error_message = "Bir Hata Var Daha Sonra Tekrar Deneyin!";
                    }
                }

            } else {
                $this->resp->error_message = "Hatalı masa seçimi";
            }
        } else {
            $this->resp->error_message = "Eksik veya Hatalı parametre gönderimi";
        }

        echo json_encode($this->resp, JSON_UNESCAPED_UNICODE);
    }

    public function DeleteAddition()
    {
        if (post('addition_id')) {
            if ($this->db->delete('addition', array('ID' => post('addition_id')))) {
                $this->resp->req_status = true;
            }
        } else {
            $this->resp->error_message = "Ekisk veya Hatalı parametre gönderimi";
        }

        echo json_encode($this->resp, JSON_UNESCAPED_UNICODE);
    }

    public function CloseAddition()
    {
        if (
            post('addition_id')
            &&
            "'" . post('payment_type') . "'"
            &&
            "'" . post('total_price') . "'"
            &&
            "'" . post('cash_price') . "'"
            &&
            "'" . post('cart_price') . "'"
            &&
            post('desk_id')
        ) {

            $paymentData = array(
                "additionID"  => post('addition_id'),
                "paymentType" => "'".post('payment_type')."'",
                "totalPrice"  => post('total_price'),
                "cashPrice"   => post('cash_price'),
                "cartPrice"   => post('cart_price'),
                "deskID"      => post('desk_id'),
            );

            if (ExecuteProcedure("prcAdditionPayment", $paymentData)) {

                reConnect();

                // Ödeme Bilgileri

                $this->db->select();
                $this->db->from('payment');
                $this->db->where('AdditionID', post('addition_id'));
                $this->resp->payment_data = $this->db->get()->result()[0];
                $this->resp->req_status = true;
            } else {
                $this->resp->error_message = "Hatalı bir durum var daha sonra tekrar dene!";
            }

        } else {
            $this->resp->error_message = ("Eksik veya Hatalı Parametre Gönderimi");
        }

        echo json_encode($this->resp, JSON_UNESCAPED_UNICODE);

    }

    function Detail($addition_id = 0){
        // Todo Adisyon detaylarını kodla

         if($addition_id > 0)
         {
            $result = $this->db->select("*")
                            ->from('vw_additionlist')
                            ->where('AdditionID', $addition_id)
                            ->get()
                            ->result();

            if(isset($result[0]))
            {
                $this->resp->req_status = true;
                $this->resp->result= $result[0];
			}
            else{
                $this->resp->error_message = "Bir sorun var ancak bende çözmeye çalışıyorum";
			}
         }
         else{
            $this->resp->error_message = "Eksik veya Hatalı Parametre Gönderimi";
		 }

         api_result($this->resp);
    }

}
