<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Method: *");
class AdditionMovement extends CI_Controller
{
    private $resp = null;

    public function __construct()
    {
        parent::__construct();
        $this->resp             = new stdClass();
        $this->resp->req_status = false;
        // varsayılan hata mesajı
        $this->resp->error_message = "Eksik veya Hatalı Parametre Gönderimi!";
    }

    public function MovementList($addition_id = 0)
    {
        if ($addition_id > 0) {

            $this->db->select("*");
            $this->db->from("vw_additionmovementlist");
            $this->db->where('AdditionID', $addition_id);
            $this->db->order_by('AdditionMovementID', 'DESC');

            $result = $this->db->get()->result();

            if (count($result) > 0) {

                $this->resp->result     = $result;
                $this->resp->req_status = true;

            } else {
                $this->resp->error_message = "Bu adisyona ait herhangi bir kayıt bulunamadı!";
            }

        }

        api_result($this->resp);
    }

    public function AddMovement()
    {
        if (
            post('product_id')
            &&
            post('addition_id')
            &&
            "'" . post("product_count") . "'"
        ) {
            $values = array(
                "productID"    => post('product_id'),
                "productCount" => post("product_count"),
                "additionID"   => post('addition_id'),
            );

            $result = SelectProcedure("prcAdditionAddProduct", $values)[0]->AdditionMovementID;

            if ($result > 0) {
                $this->resp->req_status           = true;
                $this->resp->addition_movement_id = $result;
                unset($result);
                reConnect();
                $this->db->select("*");
                $this->db->from("vw_additionmovementlist");
                $this->db->where("AdditionMovementID", $this->resp->addition_movement_id);
                $this->resp->movementListItem = $this->db->get()->result()[0];

                reConnect();
                $this->db->select("*");
                $this->db->from('vw_additionlist');
                $this->db->where('AdditionID', post('addition_id'));
                $this->resp->current_addition_info = $this->db->get()->result()[0];

            } else {
              $this->resp->error_message = "Bir Hata Var Lütfen Daha Sonra Tekrar Deneyiniz";
            }
        } else {
            $this->resp->error_message = "Eksik veya Hatalı Parametre Gönderimi";
        }

        api_result($this->resp);
    }
}
