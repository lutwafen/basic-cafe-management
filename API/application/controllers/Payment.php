<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Method: *");
class Payment extends CI_Controller
{
    private $resp = null;
    public function __construct()
    {
        parent::__construct();
        $this->resp             = new stdClass();
        $this->resp->req_status = false;
    }

    function list() {

        $select_case = '
				(
					CASE
						WHEN PaymentType = 0 THEN "Kart"
						WHEN PaymentType = 1 THEN "Nakit"
						WHEN PaymentType = 2 THEN "Kart + Nakit"
					END
				) AS PaymentTypeValue
			';

        $result = $this->db
            ->select("*, " . $select_case)
            ->from('payment')
            ->order_by('ID', 'DESC')
            ->get()
            ->result();

        if (count($result) > 0) {
            $this->resp->req_status = true;
            reConnect();
            foreach ($result as $item) {
                if ($item->AdditionID > 0) {
                    $item->addition_info = $this->db
                        ->select("*")
                        ->from("vw_additionlist")
                        ->where('AdditionID', $item->AdditionID)
                        ->get()->result()[0];
                } else {
                    $item->addition_info = "Adisyon Bilgisi bulunamad� ama hesap �denmi� garip bir sorun var";
                }
            }
            $this->resp->payment_list = $result;
        } else {
            $this->resp->error_message = "Herhangi bir �deme ge�mi�i bulunamad�!";
        }

        api_result($this->resp);
    }

    public function single($addition_id)
    {
        if ($addition_id > 0) {
            $this->db->select("*");
            $this->db->from("payment");
            $this->db->where('AdditionID', $addition_id);

            $result = $this->db->get()->result();

            if (isset($result[0])) {
                $result                 = $result[0];
                $this->resp->req_status = true;
                $this->resp->result     = $result;
            } else {
                $this->resp->error_message = "Bu adisyona ait bir ödeme bilgisi bulunamadı!";
            }

        } else {
            $this->resp->error_message = "Adisyon Numarası Sıfırdan Büyük Olmalıdır";
        }

        api_result($this->resp);
    }

}
