<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Request-Method: *");

class Reservation extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Mreservation');
	}

	function GetReservationList($filter = "ALL", $started_date = '', $finished_date = '') {

		$filter = strtoupper($filter);

		$response = new stdClass();
		$response->req_status = true;
		switch ($filter) {
		case "ALL":
			$filter_value = -1;
			break;

		case "COME":
			$filter_value = 0;
			break;

		case "NOTCOME":
			$filter_value = 1;
			break;

		default:
			$response->error_message = 'Geçerli Bir Filtreleme Kullanınız';
			$response->req_status = false;
			break;
		}

		if ($response->req_status) {
			$model_data = [
				'filter' => $filter_value,
				'started_date' => $started_date,
				'finished_date' => $finished_date,
			];

			$response->result = $this->Mreservation->get_reservation_list($model_data);
			if (count($response->result) < 1) {
				unset($response->result);
				$response->error_message = "Herhangi bir veri bulunamadı!";
				$response->req_status = false;
			}
		}

		echo json_encode($response, JSON_UNESCAPED_UNICODE);

	}

	function GetReservation($reservation_id = 0) {
		$response = new stdClass();

		if ($reservation_id > 0) {

			if (is_numeric($reservation_id)) {
				$res = $this->Mreservation->get_reservation($reservation_id);

				if ($res) {
					$response->result = $res[0];
					$response->req_status = true;
				} else {
					$response->error_message = "Veri Bulunamadı!";
					$response->req_status = false;
				}

			} else {
				$response->error_message = "ID değeri sadece numerik ifadelerden oluşabilir";
				$response->req_status = false;
			}

		} else {
			$response->error_message = "Rezervasyon ID'si 0'dan büyük olmak zorundadır";
			$response->req_status = false;
		}

		echo json_encode($response, JSON_UNESCAPED_UNICODE);
	}

	function CreateReservation() {
		$resp = new stdClass();
		$resp->req_status = false;
		if (
			isset($_POST['customer_name'])
			&& isset($_POST['reservation_date'])
			&& isset($_POST['reservation_time'])
		) {
			$create_data = array(
				"CustomerName" => $_POST['customer_name'],
				"ReservationDate" => $_POST['reservation_date'],
				"ReservationTime" => $_POST['reservation_time'],
				"CustomerNote" => isset($_POST['customer_note']) ? $_POST['customer_note'] : "",
			);
			$result = $this->Mreservation->create_reservation($create_data);
			if ($result > 0) {
				$resp->req_status = true;
				$resp->insert_id = $result;
			} else {
				$resp->error_message = "Bir hata oluştu var daha sonra tekrar deneyin!";
			}
		} else {
			$resp->error_message = "Lütfen Parametreleri Eksizsiz Bir Biçimde Gönderin";
		}

		echo json_encode($resp, JSON_UNESCAPED_UNICODE);
	}

	function UpdateReservation() {
		$resp = new stdClass();
		$resp->req_status = false;

		if (
			post('customer_name') &&
			post('reservation_date') &&
			post('reservation_time') &&
			post('reservation_id')
		) {
			if (post('reservation_id') > 0) {
				if ($this->Mreservation->update_delete_reservation("UPDATE", array(
					"CustomerName" => post('customer_name'),
					"ReservationDate" => post('reservation_date'),
					"ReservationTime" => post('reservation_time'),
				),
					array("ID" => post('reservation_id'))
				)) {
					$resp->req_status = true;
				} else {
					$resp->error_message = "Bir hata var daha sonra tekrar deneyin!";
				}
			} else {
				$resp->error_message = "Rezervasyon ID'si 0'dan büyük olmak zorundadır";
			}
		} else {
			$resp->error_message = "Lütfen Parametreleri Eksizsiz Bir Biçimde Gönderin";
		}

		echo json_encode($resp, JSON_UNESCAPED_UNICODE);
	}
	function ChangeReservationState() {

		$resp = new stdClass();
		$resp->req_status = false;

		if (
			post('reservation_id') > 0
			&&
			strlen("'".post('change_value')."'") > 0
		) {
			if ($this->Mreservation->update_delete_reservation("UPDATE", array(
				"IsComing" => "'".post('change_value')."'",
			),
				array("ID" => post('reservation_id'))
			)) {
				$resp->req_status = true;
			} else {
				$resp->error_message = "Bir hata var daha sonra tekrar deneyin!";
			}
		} else {
			$resp->error_message = "Hatalı veya Eksik Parametre Gönderdiniz";
		}

		echo json_encode($resp, JSON_UNESCAPED_UNICODE);
	}
	function RemoveReservation() {
		$resp = new stdClass();
		$resp->req_status = false;

		if (post("reservation_id") > 0) {
			if ($this->Mreservation->update_delete_reservation("DELETE", array(),
				array("ID" => post('reservation_id'))
			)) {
				$resp->req_status = true;
			} else {
				$resp->error_message = "Bir hata var daha sonra tekrar deneyin!";
			}
		} else {
			$resp->error_message = "Eksik Parametre Gönderdiniz!";
		}

		echo json_encode($resp, JSON_UNESCAPED_UNICODE);

	}

}