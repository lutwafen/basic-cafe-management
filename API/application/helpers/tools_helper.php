<?php

function post($param)
{
    return isset($_POST[$param]) ? $_POST[$param] : false;
}

function reConnect()
{
    $ci = get_instance();
    $ci->db->close();
    $ci->db->initialize();
}

function callProcedure($prcName, $dataSize)
{
    $sql = "CALL $prcName(";
    for ($i = 0; $i < $dataSize; $i++) {
        $sql .= "?,";
    }
    $sql = rtrim($sql, ",");
    $sql .= ")";
    return $sql;
}

function ExecuteProcedure($prcName, $prcData = array())
{
    reConnect();
    $ci = get_instance();
    if (count($prcData) > 0) {
        $procedure = callProcedure($prcName, count($prcData));
        $query     = $ci->db->query($procedure, $prcData);
        if ($query !== null) {
            return true;
        }

    }
    return false;
}

function SelectProcedure($prcName, $prcData)
{
    reConnect();
    $ci = get_instance();
    if (count($prcData) > 0) {
        $procedure = callProcedure($prcName, count($prcData));
        $query     = $ci->db->query($procedure, $prcData);
        if ($query !== null) {
            return $query->result();
        }

    }
    return false;
}

function api_result($write_object = array())
{
    echo json_encode($write_object, JSON_UNESCAPED_UNICODE);
}
