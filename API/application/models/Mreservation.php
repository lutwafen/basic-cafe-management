
<?php 
    class Mreservation extends CI_Model {
        private $default_table = 'reservation';
        
        function get_reservation_list($data = array())
        {
            
            if(!empty($data))
            {
                $this->db->select('*');
                $this->db->from('reservation');
                
                if($data['filter'] >= 0 && $data['filter'] < 2)
                {
                    $this->db->where('IsComing', $data['filter']); 
                }
                    
                if(strlen($data['started_date']) > 0 && strlen($data['finished_date']) > 0)
                {
                    $this->db->where('ReservationDate >=', $data['started_date']);
                    $this->db->where('ReservationDate <=', $data['finished_date']);
                }
                else if(strlen($data['started_date']) > 0)
                    $this->db->where('ReservationDate >=', $data['started_date']);
                else if(strlen($data['finished_date']) > 0)
                    $this->db->where('ReservationDate <=', $data['finished_date']);
                    
                    
                $this->db->order_by('ID', 'DESC');
                
                $result = $this->db->get();
                
                return $result->result();
            }
            
            
            return false;
        }
        
        function get_reservation($reservation_id)
        {
            if($reservation_id > 0)
            {
                $this->db->select("*");
                $this->db->from("reservation");
                $this->db->where('ID', $reservation_id);
                $result = $this->db->get();
                return $result->result();
            }
            
            
            return false;
        }
        
        function create_reservation($data)
        {
            if(is_array($data))
            {
                if($this->db->insert("reservation", $data))
                    return $this->db->insert_id();
            }
            
            return false;
        }
        
        
        function update_delete_reservation($type ="UPDATE", $data = array(), $where = array())
        {
            
            if(is_array($data) && is_array($where))
            {
                switch($type){
                    case "UPDATE":
                        $this->db->where($where);
                        return $this->db->update("reservation", $data);
                       
                    break;
                        
                    case "DELETE":
                        return $this->db->delete("reservation", $where);
                    break;
                }   
            }
            
            
            return false;
        }
        
    }