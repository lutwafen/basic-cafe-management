import Vuex from "vuex";

//Modules
import reservationModule from "./modules/Reservation.js";
import deskModule from "./modules/Desk";
import productModule from "./modules/Product";
import additionModue from "./modules/Addition";
import paymentModule from "./modules/Payment";

const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    modules: {
      reservation: reservationModule,
      desk: deskModule,
      product: productModule,
      addition: additionModue,
      payment: paymentModule
    }
  });
};

export default createStore;
