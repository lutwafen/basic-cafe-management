const state = {
  paymentList : []
};
const mutations = {
  setPaymentList(state, payload){
    state.paymentList.push(payload);
  },
  initPaymentList(state, payload){
    state.paymentList = payload;
  }
};
const getters = {
  getPaymentList(state){
    return state.paymentList;
  }
};
const actions = {
  paymentForAddition(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      var paymentData = new FormData();
      paymentData.append("addition_id", payload.AdditionID);
      paymentData.append("payment_type", payload.PaymentType);
      paymentData.append("total_price", payload.TotalPrice);
      paymentData.append("cash_price", payload.CashPrice);
      paymentData.append("cart_price", payload.CartPrice);
      paymentData.append("desk_id", payload.DeskID);

      this.$axios.post('/Addition/CloseAddition', paymentData)
        .then(result => {
          result = result.data;
          if(result.req_status === true)
          {
            resolve(result.payment_data);
          }
          else{
            reject(result.error_message);
          }
        })
        .catch(err => {
          console.log(err);
        })


    });
  },
  getPaymentData(vuexContext, payload){
    return new Promise((resolve, reject) => {
      this.$axios.get('/Payment/single/' + payload)
      .then(result => {
        result = result.data;
        if(result.req_status == true)
        {
          resolve(result.result);
        }
        else{
          reject(result.error_message);
        }
      })
      .catch(err => {
        console.log(err);
      })

    });
  },
  paymentList(vuexContext){
    return new Promise((resolve, reject) => {
      this.$axios.get('/Payment/List').then((result) => {
        result = result.data;
        if(result.req_status === true){
          vuexContext.commit('initPaymentList', result.payment_list);
          resolve(result.payment_list);
        }
        else
          reject(result.error_message);
      })
      .catch(err => {
        console.error(err);
      })
    });
  }
};

export default {
  state,
  mutations,
  getters,
  actions
};
