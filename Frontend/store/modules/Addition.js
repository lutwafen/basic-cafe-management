import actions from "./Addition/Actions";
import mutations from "./Addition/Mutations";
import getters from "./Addition/Getters";
import state from "./Addition/State";

export default { state, mutations, getters, actions };
