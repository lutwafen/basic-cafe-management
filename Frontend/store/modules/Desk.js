const state = {
  deskList: []
};
const getters = {
  getDeskList(state) {
    return state.deskList;
  }
};
const mutations = {
  initDeskList(state, payload) {
    state.deskList = payload;
  },
  addDeskItem(state, payload) {
    state.deskList.push(payload);
  },
  updateDeskItem(state, payload) {
    let desk_index = state.deskList.findIndex(
      r_index => r_index.ID == payload.ID
    );

    if (desk_index > -1) {
      state.deskList.splice(desk_index, 1, payload);
    }
  },
  removeDeskItem(state, payload) {
    let desk_index = state.deskList.findIndex(
      r_index => r_index.ID == payload.ID
    );

    if (desk_index > -1) {
      state.deskList.splice(desk_index, 1);
    }
  }
};
const actions = {
  nuxtServerInit(vuexContext, context) {
    return context.$axios.get("/Desk/List").then(response => {
      // console.log(response.data);
      vuexContext.commit("initDeskList", response.data);
    });
  },
  createDeskItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      let formdata = new FormData();
      formdata.append("desk_title", payload.desk_title);
      formdata.append("desk_position", payload.desk_position);

      this.$axios
        .post("/Desk/Create", formdata)
        .then(resp => {
          resp = resp.data;

          if (resp.req_status == true) {
            vuexContext.commit("addDeskItem", resp.created_item);
            resolve("Başarıyla Oluşturuldu!");
          } else {
            reject(resp.error_message);
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  },
  updateDeskItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      let formdata = new FormData();
      formdata.append("desk_id", payload.id);
      formdata.append("desk_title", payload.desk_title);
      formdata.append("desk_position", payload.desk_position);
      formdata.append("desk_is_open", payload.desk_is_open);

      this.$axios
        .post("/Desk/Update", formdata)
        .then(resp => {
          console.log(resp.data);

          if (resp.data.req_status == true) {
            vuexContext.commit("updateDeskItem", resp.data.updated_item);
            resolve({
              result_message: "Güncelleme İşlemi Başarıyla Tamamlandı!"
            });
          } else {
            reject(resp.data.error_message);
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  },
  trashDeskItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      let formdata = new FormData();
      formdata.append("desk_id", payload.ID);

      this.$axios
        .post("Desk/Delete", formdata)
        .then(resp => {
          resp = resp.data;
          if (resp.req_status == true) {
            vuexContext.commit("removeDeskItem", payload);
            resolve(
              "Başarıyla " + payload.DeskTitle + " Adlı Masa Kaldırıldı!"
            );
          } else {
            reject(resp.error_message);
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
