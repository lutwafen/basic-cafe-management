const state = {
  product_list: [],
  update_select_product: {}
};
const getters = {
  getProductList(state) {
    return state.product_list;
  },
  getUpdateSelectProduct(state) {
    return state.update_select_product;
  }
};
const mutations = {
  setProductList(state, payload) {
    state.product_list = payload;
  },
  setUpdateSelectProduct(state, payload) {
    state.update_select_product = payload;
  },
  updateProductList(state, payload) {
    let productIndex = state.product_list.findIndex(
      p => p.ProductID == payload.ProductID
    );
    if (productIndex > -1) {
      state.product_list.splice(productIndex, 1, payload);
    }
  },
  addItemToProductList(state, payload) {
    state.product_list.push(payload);
  },
  removeItemToProductList(state, payload) {
    let productIndex = state.product_list.findIndex(
      p => p.ProductID === payload.ProductID
    );

    if (productIndex > 0) {
      state.product_list.splice(productIndex, 1);
    }
  }
};
const actions = {
  async nuxtServerInit(vuexContext, context) {
    await context.$axios.get("/Product/list").then(resp => {
      resp = resp.data;
      //console.log(resp);
      if (resp.req_status) {
        vuexContext.commit("setProductList", resp.product_list);
      } else {
        console.log(resp.error_message);
      }
    });
  },
  setUpdateProductItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      vuexContext.commit("setUpdateSelectProduct", payload);
      resolve(true);
    });
  },
  updateProductItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      let update_data = new FormData();
      update_data.append("product_id", payload.ProductID);
      update_data.append("product_title", payload.Title);
      update_data.append("type", payload.Type);
      update_data.append("purchase_price", payload.Purchase);
      update_data.append("sale_price", payload.Sale);
      this.$axios
        .post("/product/editproductitem", update_data)
        .then(resp => {
          if (resp.data.req_status == true) {
            payload.PriceDiff = payload.Sale - payload.Purchase;
            vuexContext.commit("updateProductList", payload);
            resolve(true);
          } else {
            reject({ error_message: resp.data.error_message });
          }
        })
        .catch(er => {
          console.log(er);
        });
    });
  },
  createProductItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      let product_item = new FormData();
      product_item.append("product_name", payload.product_title);
      product_item.append("sale_price", payload.product_sale_price);
      product_item.append("purchase_price", payload.product_purchase_price);
      product_item.append("type", payload.product_type);

      this.$axios.post("Product/CreateProduct", product_item).then(result => {
        result = result.data;
        if (result.req_status == true) {
          let item = {
            ProductID: result.product_id,
            Title: payload.product_title,
            Purchase: payload.product_purchase_price,
            Sale: payload.product_sale_price,
            PriceDiff:
              payload.product_sale_price - payload.product_purchase_price,
            Type: payload.product_type,
            ProductType: payload.product_type === 0 ? "Yiyecek " : "İçecek"
          };
          vuexContext.commit("addItemToProductList", item);
          resolve(true);
        } else {
          reject({
            error_message: result.error_message
          });
        }
      });
    });
  },
  removeProductItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      let product_item = new FormData();
      product_item.append("product_id", payload.ProductID);
      this.$axios
        .post("/Product/RemoveProductItem", product_item)
        .then(resp => {
          resp = resp.data;
          if (resp.req_status === true) {
            vuexContext.commit("removeItemToProductList", payload);
            resolve(true);
          } else {
            reject(resp.error_message);
          }
        });
    });
  }
};

export default { state, getters, mutations, actions };
