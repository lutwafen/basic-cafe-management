const state = {
  reservationlist: [],
  cameReservationList: [],
  waitingReservationList: [],
  filteredList: [],
  updateItem: {}
};

const mutations = {
  setReservationList(state, payload) {
    state.reservationlist = payload;
  },
  addReservationList(state, payload) {
    state.reservationlist.push(payload);
  },
  updateReservationStatus(state, payload) {
    let reservationIndex = state.reservationlist.findIndex(
      r_index => r_index.ID == payload.ID
    );

    if (reservationIndex > -1) {
      state.reservationlist.splice(reservationIndex, 1, payload);
    }
  },
  trashReservationItem(state, payload) {
    let reservationIndex = state.reservationlist.findIndex(
      r_index => r_index.ID == payload.ID
    );

    if (reservationIndex > -1) {
      state.reservationlist.splice(reservationIndex, 1);
    }
  },
  setFilteredList(state, payload) {
    state.reservationList = payload;
  },
  setUpdateItem(state, payload) {
    state.updateItem = payload;
  }
};

const getters = {
  getReservationList(state) {
    return state.reservationlist;
  },
  getWaitingReservation(state) {
    // Gereksiz bir getter, canın isterse düzelt
    let reserv_list = [];
    state.reservationlist.forEach(item => {
      if (item.IsComing == 1) {
        reserv_list.push(item);
      }
    });

    return reserv_list;
  },
  // hangi kafayla yazdın 🤕? getCameReservationList(state) {},
  getFilteredList(state) {
    //?!?!?!?!?!? Nerede Kullandın? Kontrol et
    return state.reservationList;
  },
  getUpdatedItem(state) {
    return state.updateItem;
  }
};

const actions = {
  nuxtServerInit(vuexContext, context) {
    return context.$axios
      .get("/Reservation/GetReservationList")
      .then(response => {
        if (response.data.req_status) {
          vuexContext.commit("setReservationList", response.data.result);
        }
      });
  },
  changeReservationStatus(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      var send_form_data = new FormData();
      send_form_data.append("change_value", payload.change_value);
      send_form_data.append("reservation_id", payload.ID);

      this.$axios
        .post("/Reservation/ChangeReservationState", send_form_data)
        .then(resp => {
          resp = resp.data;
          if (resp.req_status == true) {
            payload.IsComing = 0;
            vuexContext.commit("updateReservationStatus", payload);
          } else {
            console.log(resp.error_message);
          }

          resolve({
            status: resp.req_status,
            message: resp.status
              ? "Rezervasyon Durumu Başarıyla Güncelleştirildi!"
              : resp._error_message
          });
        })
        .catch(err => {
          console.error(err);
          reject(err);
        });
    });
  },
  trashReservationItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      var formdata = new FormData();
      formdata.append("reservation_id", payload.ID);

      this.$axios
        .post("/Reservation/RemoveReservation", formdata)
        .then(resp => {
          resp = resp.data;

          if ((resp.req_status = true)) {
            vuexContext.commit("trashReservationItem", payload);
          } else {
            console.error(resp.error_message);
          }

          resolve({
            status: resp.req_status,
            message: resp.status
              ? "Başarıyla Rezervasyon İşlemi Silindi!"
              : resp._error_message
          });
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
    });
  },
  createReservationItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      var formdata = new FormData();
      formdata.append("customer_name", payload.customerFullname);
      formdata.append("reservation_date", payload.reservationDate);
      formdata.append("reservation_time", payload.reservationTime);
      if (
        payload.reservationNote == "Herhangi bir Not" ||
        payload.reservationNote == ""
      ) {
        payload.reservationNote = "Not bilgisi girilmemiş";
      }
      formdata.append("customer_note", payload.reservationNote);
      this.$axios
        .post("/Reservation/CreateReservation", formdata)
        .then(resp => {
          resp = resp.data;
          let reservation_id = resp.insert_id;

          if (resp.req_status) {
            let reservation_item = {
              ID: reservation_id,
              CustomerName: payload.customerFullname,
              ReservationDate: payload.reservationDate,
              ReservationTime: payload.reservationTime,
              IsComing: "1",
              CustomeNote: payload.reservationNote
            };
            vuexContext.commit("addReservationList", reservation_item);
          }

          resolve({
            status: resp.req_status,
            message: resp.req_status
              ? "Rezervasyon Başarıyla Oluşturuldu!"
              : resp.error_message
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  },

  filteredList(vuexContext, filteredValue) {
    return new Promise((resolve, reject) => {
      let formdata = new FormData();

      let selection_value = "ALL";
      if (filteredValue.selection_type > -1) {
        selection_value =
          filteredValue.selection_type == 0 ? "COME" : "NOTCOME";
      }

      let url = "/Reservation/GetReservationList/" + selection_value;

      if (filteredValue.started_date.length > 0)
        url += "/" + filteredValue.started_date;
      if (filteredValue.finished_date.length > 0)
        url += "/" + filteredValue.finished_date;

      if (
        filteredValue.started_date == "" &&
        filteredValue.finished_date.length > 0
      ) {
        url =
          "/Reservation/GetReservationList/" +
          selection_value +
          "/2020-01-01/" +
          filteredValue.finished_date;
      }
      this.$axios
        .get(url)
        .then(resp => {
          resp = resp.data;
          // console.log(resp);

          if (resp.req_status == true) {
            vuexContext.commit("setReservationList", resp.result);
            resolve(resp);
          } else {
            reject(resp.error_message);
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  },
  setUpdateItem(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      vuexContext.commit("setUpdateItem", payload);
      resolve(true);
      reject("bir hata olabilir mi acaba?");
    });
  },

  updateReservationInfo(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      let formdata = new FormData();
      formdata.append("customer_name", payload.CustomerName);
      formdata.append("reservation_date", payload.ReservationDate);
      formdata.append("reservation_time", payload.ReservationTime);
      formdata.append("reservation_id", payload.ID);

      this.$axios
        .post("/Reservation/UpdateReservation", formdata)
        .then(resp => {
          resp = resp.data;
          if (resp.req_status == true) {
            resolve(true);
          } else {
            reject(resp.error_message);
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
};

export default {
  state,
  mutations,
  getters,
  actions
};
