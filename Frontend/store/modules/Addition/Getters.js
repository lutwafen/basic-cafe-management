export default {
  getAdditionList(state) {
    return state.additionList;
  },
  getOpenAdditionList(state) {
    return state.additionList.filter(function(item) {
      return item.Status === "1";
    });
  },
  getAdditionMovementList(state) {
    return state.additionMovementList || [];
  }
};
