export default {
  nuxtServerInit(vuexContext, context) {
    return new Promise((resolve, reject) => {
      //Addition/AdditionList
      //AdditionMovement/MovementList(addition_id)

      context.$axios
        .get("/Addition/AdditionList")
        .then(resp => {
          resp = resp.data;
          //let ad_list = [];
          vuexContext.commit("setAdditionList", resp);
          resolve(true);
        })
        .catch(err => {
          reject(err);
          console.log(err);
        });
    });
  },
  additionMovementList(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get("AdditionMovement/MovementList/" + payload)
        .then(result => {
          result = result.data;
          if (result.req_status === true) {
            vuexContext.commit("additionMovementList", result.result);
            resolve(true);
          } else {
            console.log(result.error_message);
            reject(result.error_message);
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  },
  createAddition(vuexContext, payload) {
    return new Promise((resolve, reject) =>{
      let addition_params = new FormData();
      addition_params.append('desk_id', payload);
      this.$axios.post('/Addition/CreateAddition', addition_params)
          .then((result) => {
            result = result.data;
            if(result.req_status === true)
            {
              vuexContext.commit('addToAdditionList', {AdditionID: result.addition_id});
              resolve(result.addition_id)
            }
            else {
                reject(result.error_message);
            }
          })
          .catch(err => {
            console.log(err);
          })
    });
  },
  createAdditionMovement(vuexContext, payload) {
    return new Promise((resolve, reject) => {
      //Mutation: addAdditionMovementList

      var additionMovementData = new FormData();
      additionMovementData.append("addition_id", payload.AdditionID);
      additionMovementData.append("product_id", payload.ProductID);
      additionMovementData.append("product_count", payload.ProductCount);

      this.$axios
        .post("/AdditionMovement/AddMovement", additionMovementData)
        .then(result => {
          result = result.data;
          console.log(result);
          if (result.req_status === true) {
            vuexContext.commit(
              "addAdditionMovementList",
              result.movementListItem
            );
            //current_addition_info

            vuexContext.commit("updateAddition", result.current_addition_info);

            resolve({
              currentAddition: result.current_addition_info,
              movementListItem: result.movementListItem,
              movementID: result.addition_movement_id,
              success: true
            });
          } else {
            let error_message = result.error_message;
            reject({ success: false, message: error_message });
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
};
