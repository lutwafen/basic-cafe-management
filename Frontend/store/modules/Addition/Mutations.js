export default {
  setAdditionList(state, payload) {
    state.additionList = payload;
  },
  additionMovementList(state, payload) {
    state.additionMovementList = payload;
  },
  addToAdditionList(state, payload) {
    state.additionList.unshift(payload);
  },
  addAdditionMovementList(state, payload) {
    state.additionMovementList.unshift(payload);
  },
  updateAddition(state, payload) {
    let additionIndex = state.additionList.findIndex(addition_item =>
      addition_item.AdditionID === payload.AdditionID
    );

    if (additionIndex > -1) {
      state.additionList.splice(additionIndex, 1, payload);
    }
  }
};
