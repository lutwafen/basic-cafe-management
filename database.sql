-- --------------------------------------------------------
-- Sunucu:                       127.0.0.1
-- Sunucu sürümü:                10.4.8-MariaDB - mariadb.org binary distribution
-- Sunucu İşletim Sistemi:       Win64
-- HeidiSQL Sürüm:               10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- tablo yapısı dökülüyor odev_db.addition
CREATE TABLE IF NOT EXISTS `addition` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DeskID` int(11) DEFAULT NULL,
  `TotalPrice` decimal(12,2) DEFAULT 0.00,
  `PayStatus` tinyint(4) DEFAULT 0,
  `Status` tinyint(4) DEFAULT 1,
  `ActiveDatetime` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;

-- odev_db.addition: ~30 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `addition` DISABLE KEYS */;
INSERT INTO `addition` (`ID`, `DeskID`, `TotalPrice`, `PayStatus`, `Status`, `ActiveDatetime`) VALUES
	(1, 1, 380.97, 1, 0, '2020-03-02 08:38:52'),
	(2, 2, 284.97, 1, 0, '2020-03-02 08:38:52'),
	(6, 4, 37.00, 1, 0, '2020-03-20 13:27:56'),
	(8, 2, 61.00, 1, 0, '2020-03-20 13:44:08'),
	(9, 2, 11.00, 1, 0, '2020-03-21 23:47:20'),
	(12, 5, 22.00, 1, 0, '2020-03-22 00:18:11'),
	(13, 4, 13.00, 1, 0, '2020-03-22 00:18:35'),
	(14, 1, 24.00, 1, 0, '2020-03-22 00:19:58'),
	(15, 4, 13.00, 1, 0, '2020-03-22 00:24:22'),
	(16, 5, 13.00, 1, 0, '2020-03-22 00:26:35'),
	(17, 4, 11.00, 1, 0, '2020-03-22 00:28:28'),
	(18, 4, 22.00, 1, 0, '2020-03-22 00:39:20'),
	(19, 1, 26.00, 1, 0, '2020-03-22 00:42:40'),
	(32, 4, 46.00, 1, 0, '2020-04-03 13:46:58'),
	(33, 5, 22.00, 1, 0, '2020-04-09 21:59:03'),
	(34, 2, 0.00, 1, 0, '2020-04-09 22:06:15'),
	(35, 4, 26.00, 1, 0, '2020-04-09 22:13:55'),
	(36, 2, 0.00, 1, 0, '2020-04-09 22:14:39'),
	(37, 2, 33.00, 1, 0, '2020-04-09 22:16:13'),
	(38, 2, 22.00, 1, 0, '2020-04-14 23:39:15'),
	(39, 2, 13.00, 1, 0, '2020-04-14 23:39:58'),
	(40, 1, 26.00, 1, 0, '2020-04-14 23:44:53'),
	(41, 4, 122.00, 1, 0, '2020-04-14 23:56:35'),
	(42, 7, 265.75, 1, 0, '2020-04-15 00:00:48'),
	(43, 7, 10.25, 1, 0, '2020-04-22 15:55:57'),
	(44, 2, 30.75, 1, 0, '2020-04-22 16:34:45'),
	(45, 1, 30.00, 1, 0, '2020-04-22 17:11:22'),
	(46, 8, 162.00, 1, 0, '2020-04-22 17:13:45'),
	(47, 5, 10.25, 0, 1, '2020-04-22 19:30:23'),
	(48, 7, 10.25, 0, 1, '2020-04-22 19:44:32'),
	(49, 1, 10.25, 1, 0, '2020-04-22 19:44:52');
/*!40000 ALTER TABLE `addition` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.additionmovement
CREATE TABLE IF NOT EXISTS `additionmovement` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AdditionID` int(11) DEFAULT NULL,
  `MovementTitle` varchar(64) DEFAULT NULL,
  `PreparationStatus` tinyint(4) DEFAULT 0,
  `Price` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4;

-- odev_db.additionmovement: ~88 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `additionmovement` DISABLE KEYS */;
INSERT INTO `additionmovement` (`ID`, `AdditionID`, `MovementTitle`, `PreparationStatus`, `Price`) VALUES
	(1, 1, 'Meyve Tabağı', 1, 49.99),
	(3, 1, 'Meyve Tabağı', 1, 49.99),
	(4, 1, 'Meyve Tabağı 2', 1, 49.99),
	(5, 2, 'Meyve Tabağı', 1, 49.99),
	(6, 2, 'Meyve Tabağı 2', 1, 49.99),
	(7, 2, 'Test Verisi', 1, 11.00),
	(9, 1, 'Meyve Tabağı', 1, 26.00),
	(10, 1, 'Meyve Suyu', 1, 22.00),
	(11, 1, 'Meyve Suyu', 1, 22.00),
	(15, 2, 'Meyve Tabağı', 0, 26.00),
	(16, 1, 'Meyve Tabağı', 0, 13.00),
	(17, 2, 'Meyve Tabağı', 0, 52.00),
	(19, 1, 'Meyve Tabağı', 0, 39.00),
	(20, 1, 'Meyve Suyu', 0, 33.00),
	(25, 1, 'Meyve Tabağı', 0, 26.00),
	(27, 1, 'Meyve Tabağı', 0, 26.00),
	(28, 1, 'Meyve Suyu', 0, 22.00),
	(29, 1, 'Meyve Suyu', 0, 22.00),
	(30, 1, 'Meyve Tabağı', 0, 26.00),
	(31, 1, 'Meyve Suyu', 0, 22.00),
	(32, 2, 'Meyve Tabağı', 0, 13.00),
	(33, 2, 'Meyve Suyu', 0, 22.00),
	(34, 1, 'Meyve Tabağı', 0, 26.00),
	(35, 1, 'Meyve Suyu', 0, 11.00),
	(36, 1, 'Meyve Tabağı', 0, 13.00),
	(37, 1, 'Meyve Suyu', 0, 11.00),
	(40, 6, 'Kahve', 0, 2.00),
	(41, 6, 'Meyve Tabağı', 0, 13.00),
	(42, 8, 'Meyve Tabağı', 0, 13.00),
	(43, 8, 'Meyve Suyu', 0, 22.00),
	(44, 8, 'Meyve Tabağı', 0, 26.00),
	(45, 6, 'Meyve Suyu', 0, 22.00),
	(46, 9, 'Meyve Suyu', 0, 11.00),
	(47, 12, 'Meyve Suyu', 0, 22.00),
	(48, 13, 'Meyve Tabağı', 0, 13.00),
	(49, 14, 'Meyve Tabağı', 0, 13.00),
	(50, 14, 'Meyve Suyu', 0, 11.00),
	(51, 15, 'Meyve Tabağı', 0, 13.00),
	(52, 16, 'Meyve Tabağı', 0, 13.00),
	(53, 17, 'Meyve Suyu', 0, 11.00),
	(54, 18, 'Meyve Suyu', 0, 22.00),
	(55, 19, 'Meyve Tabağı', 0, 26.00),
	(56, 32, 'Meyve Suyu', 0, 22.00),
	(57, 32, 'Meyve Suyu', 0, 22.00),
	(58, 32, 'Kahve', 0, 2.00),
	(59, 33, 'Meyve Suyu', 0, 22.00),
	(60, 34, 'Nar', 0, 0.00),
	(61, 35, 'Meyve Tabağı', 0, 26.00),
	(62, 36, 'Nar', 0, 0.00),
	(63, 37, 'Nar', 0, 22.00),
	(64, 37, 'Meyve Suyu', 0, 11.00),
	(65, 38, 'Meyve Suyu', 0, 22.00),
	(66, 39, 'Meyve Tabağı', 0, 13.00),
	(67, 40, 'Meyve Tabağı', 0, 26.00),
	(68, 41, 'Meyve Tabağı', 0, 52.00),
	(69, 41, 'Meyve Suyu', 0, 22.00),
	(70, 41, 'Meyve Suyu', 0, 22.00),
	(71, 41, 'Meyve Tabağı', 0, 26.00),
	(72, 42, 'Meyve Suyu', 0, 22.00),
	(73, 42, 'Meyve Suyu', 0, 22.00),
	(74, 42, 'Nar', 0, 25.00),
	(75, 42, 'Başka bir Kahve', 0, 20.50),
	(76, 42, 'Kahve', 0, 2.00),
	(77, 42, 'Meyve Suyu', 0, 11.00),
	(78, 42, 'Su', 0, 2.00),
	(79, 42, 'Su', 0, 1.00),
	(80, 42, 'Su', 0, 1.00),
	(81, 42, 'Başka bir Kahve', 0, 10.25),
	(82, 42, 'Su', 0, 1.00),
	(83, 42, 'Kahve', 0, 2.00),
	(84, 42, 'Test Ürünü 2', 0, 30.00),
	(85, 42, 'Başka bir Kahve', 0, 10.25),
	(86, 42, 'Nar', 0, 25.00),
	(87, 42, 'Nar', 0, 25.00),
	(88, 42, 'Başka bir Kahve', 0, 10.25),
	(89, 42, 'Başka bir Kahve', 0, 10.25),
	(90, 42, 'Başka bir Kahve', 0, 10.25),
	(91, 42, 'Nar', 0, 25.00),
	(92, 43, 'Başka bir Kahve', 0, 10.25),
	(93, 44, 'Başka bir Kahve', 0, 30.75),
	(94, 45, 'Test Ürünü 2', 0, 30.00),
	(95, 46, 'Başka bir Kahve', 0, 10.25),
	(96, 46, 'Test Ürünü 2', 0, 30.00),
	(97, 46, 'Başka bir Kahve', 0, 10.25),
	(98, 46, 'Meyve Suyu', 0, 11.00),
	(99, 46, 'Nar', 0, 25.00),
	(100, 46, 'Başka bir Kahve', 0, 10.25),
	(101, 46, 'Nar', 0, 25.00),
	(102, 46, 'Test Ürünü 2', 0, 30.00),
	(103, 47, 'Başka bir Kahve', 0, 10.25),
	(104, 48, 'Başka bir Kahve', 0, 10.25),
	(105, 49, 'Başka bir Kahve', 0, 10.25),
	(106, 46, 'Başka bir Kahve', 0, 10.25);
/*!40000 ALTER TABLE `additionmovement` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.additionmovementproduct
CREATE TABLE IF NOT EXISTS `additionmovementproduct` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) DEFAULT NULL,
  `ProductCount` tinyint(3) unsigned DEFAULT NULL,
  `DescText` varchar(255) DEFAULT NULL,
  `AdditionMovementID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4;

-- odev_db.additionmovementproduct: ~88 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `additionmovementproduct` DISABLE KEYS */;
INSERT INTO `additionmovementproduct` (`ID`, `ProductID`, `ProductCount`, `DescText`, `AdditionMovementID`) VALUES
	(1, 1, 1, 'TEST', 1),
	(2, 2, 1, 'TEST', 3),
	(3, 2, 1, 'test', 4),
	(4, 2, 1, 'TEST', 5),
	(5, 2, 1, 'test', 6),
	(6, 2, 1, 'test', 7),
	(7, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 9),
	(8, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 10),
	(9, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 11),
	(13, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 15),
	(14, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 16),
	(15, 1, 4, 'Meyve Tabağı Adlı Üründen 4 Adet Sipariş Verilmiştir; Toplam : 52.00 ₺ Tutmuştur', 17),
	(17, 1, 3, 'Meyve Tabağı Adlı Üründen 3 Adet Sipariş Verilmiştir; Toplam : 39.00 ₺ Tutmuştur', 19),
	(18, 2, 3, 'Meyve Suyu Adlı Üründen 3 Adet Sipariş Verilmiştir; Toplam : 33.00 ₺ Tutmuştur', 20),
	(23, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 25),
	(25, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 27),
	(26, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 28),
	(27, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 29),
	(28, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 30),
	(29, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 31),
	(30, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 32),
	(31, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 33),
	(32, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 34),
	(33, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 35),
	(34, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 36),
	(35, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 37),
	(38, 3, 1, 'Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 2.00 ₺ Tutmuştur', 40),
	(39, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 41),
	(40, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 42),
	(41, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 43),
	(42, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 44),
	(43, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 45),
	(44, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 46),
	(45, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 47),
	(46, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 48),
	(47, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 49),
	(48, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 50),
	(49, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 51),
	(50, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 52),
	(51, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 53),
	(52, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 54),
	(53, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 55),
	(54, 2, 0, 'Meyve Suyu Adlı Üründen 0 Adet Sipariş Verilmiştir; Toplam : 0.00 ₺ Tutmuştur', 56),
	(55, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 57),
	(56, 3, 1, 'Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 2.00 ₺ Tutmuştur', 58),
	(57, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 59),
	(58, 7, 2, 'Nar Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 0.00 ₺ Tutmuştur', 60),
	(59, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 61),
	(60, 7, 1, 'Nar Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 0.00 ₺ Tutmuştur', 62),
	(61, 7, 2, 'Nar Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 63),
	(62, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 64),
	(63, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 65),
	(64, 1, 1, 'Meyve Tabağı Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 13.00 ₺ Tutmuştur', 66),
	(65, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 67),
	(66, 1, 4, 'Meyve Tabağı Adlı Üründen 4 Adet Sipariş Verilmiştir; Toplam : 52.00 ₺ Tutmuştur', 68),
	(67, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 69),
	(68, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 70),
	(69, 1, 2, 'Meyve Tabağı Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 26.00 ₺ Tutmuştur', 71),
	(70, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 72),
	(71, 2, 2, 'Meyve Suyu Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 22.00 ₺ Tutmuştur', 73),
	(72, 7, 1, 'Nar Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 25.00 ₺ Tutmuştur', 74),
	(73, 8, 2, 'Başka bir Kahve Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 20.50 ₺ Tutmuştur', 75),
	(74, 3, 1, 'Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 2.00 ₺ Tutmuştur', 76),
	(75, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 77),
	(76, 21, 2, 'Su Adlı Üründen 2 Adet Sipariş Verilmiştir; Toplam : 2.00 ₺ Tutmuştur', 78),
	(77, 21, 1, 'Su Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 1.00 ₺ Tutmuştur', 79),
	(78, 21, 1, 'Su Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 1.00 ₺ Tutmuştur', 80),
	(79, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 81),
	(80, 21, 1, 'Su Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 1.00 ₺ Tutmuştur', 82),
	(81, 3, 1, 'Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 2.00 ₺ Tutmuştur', 83),
	(82, 5, 1, 'Test Ürünü 2 Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 30.00 ₺ Tutmuştur', 84),
	(83, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 85),
	(84, 7, 1, 'Nar Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 25.00 ₺ Tutmuştur', 86),
	(85, 7, 1, 'Nar Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 25.00 ₺ Tutmuştur', 87),
	(86, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 88),
	(87, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 89),
	(88, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 90),
	(89, 7, 1, 'Nar Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 25.00 ₺ Tutmuştur', 91),
	(90, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 92),
	(91, 8, 3, 'Başka bir Kahve Adlı Üründen 3 Adet Sipariş Verilmiştir; Toplam : 30.75 ₺ Tutmuştur', 93),
	(92, 5, 1, 'Test Ürünü 2 Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 30.00 ₺ Tutmuştur', 94),
	(93, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 95),
	(94, 5, 1, 'Test Ürünü 2 Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 30.00 ₺ Tutmuştur', 96),
	(95, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 97),
	(96, 2, 1, 'Meyve Suyu Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 11.00 ₺ Tutmuştur', 98),
	(97, 7, 1, 'Nar Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 25.00 ₺ Tutmuştur', 99),
	(98, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 100),
	(99, 7, 1, 'Nar Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 25.00 ₺ Tutmuştur', 101),
	(100, 5, 1, 'Test Ürünü 2 Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 30.00 ₺ Tutmuştur', 102),
	(101, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 103),
	(102, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 104),
	(103, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 105),
	(104, 8, 1, 'Başka bir Kahve Adlı Üründen 1 Adet Sipariş Verilmiştir; Toplam : 10.25 ₺ Tutmuştur', 106);
/*!40000 ALTER TABLE `additionmovementproduct` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.department
CREATE TABLE IF NOT EXISTS `department` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DepartmentValue` varchar(32) DEFAULT NULL,
  `DepartmentDesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- odev_db.department: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` (`ID`, `DepartmentValue`, `DepartmentDesc`) VALUES
	(1, 'Kasiyer', 'Kasiyer hesabı');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.desk
CREATE TABLE IF NOT EXISTS `desk` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DeskTitle` varchar(32) DEFAULT NULL,
  `DeskPosition` tinyint(4) DEFAULT NULL,
  `DeskIsOpen` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- odev_db.desk: ~6 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `desk` DISABLE KEYS */;
INSERT INTO `desk` (`ID`, `DeskTitle`, `DeskPosition`, `DeskIsOpen`) VALUES
	(1, 'Bahçe 1', 2, 0),
	(2, 'İçeri 1', 0, 0),
	(4, 'test Dışarı Masa', 1, 0),
	(5, 'Yeni Bir Masa Oluşturuyorum', 0, 1),
	(7, 'vuex test masası', 0, 1),
	(8, 'Dışarıdaki masayı içeriye taşıyo', 0, 0);
/*!40000 ALTER TABLE `desk` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(32) DEFAULT NULL,
  `Password` varchar(32) DEFAULT NULL,
  `DepartmentID` tinyint(4) DEFAULT 0,
  `Wage` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- odev_db.employee: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`ID`, `Username`, `Password`, `DepartmentID`, `Wage`) VALUES
	(1, 'fatih', '1234', 1, 1300.00);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.employeelog
CREATE TABLE IF NOT EXISTS `employeelog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeID` int(11) DEFAULT NULL,
  `LogText` text DEFAULT NULL,
  `LogDate` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- odev_db.employeelog: ~4 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `employeelog` DISABLE KEYS */;
INSERT INTO `employeelog` (`ID`, `EmployeeID`, `LogText`, `LogDate`) VALUES
	(1, 1, 'fatih Adlı Kullanıcı Sisteme Giriş Yaptı', '2020-02-20 22:21:01'),
	(2, 1, '@fatih Kullanıcı Sisteme Giriş Yaptı', '2020-02-20 23:00:11'),
	(3, 1, '@Fatih Kullanıcı Sisteme Giriş Yaptı', '2020-02-21 18:15:05'),
	(4, 1, '@fatih Kullanıcı Sisteme Giriş Yaptı', '2020-02-21 18:16:21');
/*!40000 ALTER TABLE `employeelog` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuTitle` varchar(64) DEFAULT NULL,
  `MenuPrice` decimal(12,2) DEFAULT NULL,
  `MenuTotalPrice` decimal(12,2) DEFAULT NULL,
  `IsActive` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- odev_db.menu: ~2 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`ID`, `MenuTitle`, `MenuPrice`, `MenuTotalPrice`, `IsActive`) VALUES
	(5, 'Kahve - Meyve Suyu', 30.50, 30.00, 0),
	(6, 'test', 31.25, 13.00, 0),
	(7, 'Nar + Meyve Suyu', 380.50, 137.00, 0);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.menuproduct
CREATE TABLE IF NOT EXISTS `menuproduct` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuID` int(11) DEFAULT NULL,
  `ProductID` int(11) NOT NULL,
  `Count` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;

-- odev_db.menuproduct: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `menuproduct` DISABLE KEYS */;
/*!40000 ALTER TABLE `menuproduct` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AdditionID` int(11) DEFAULT NULL,
  `TotalPrice` decimal(12,2) DEFAULT NULL,
  `CartPrice` decimal(12,2) DEFAULT NULL,
  `CashPrice` decimal(12,2) DEFAULT NULL,
  `PaymentType` tinyint(4) DEFAULT NULL,
  `PaymentDate` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

-- odev_db.payment: ~30 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`ID`, `AdditionID`, `TotalPrice`, `CartPrice`, `CashPrice`, `PaymentType`, `PaymentDate`) VALUES
	(1, 2, 223.00, 111.00, 111.00, 2, '2020-02-29 02:41:17'),
	(2, 1, 509.00, 509.00, 0.00, 0, '2020-02-29 02:43:35'),
	(3, 8, 61.00, 0.00, 61.00, 1, '2020-03-21 23:29:24'),
	(4, 6, 37.00, 18.00, 18.00, 2, '2020-03-21 23:29:36'),
	(5, 9, 11.00, 0.00, 11.00, 1, '2020-03-21 23:47:32'),
	(6, 12, 45.00, 45.00, 0.00, 0, '2020-03-22 00:18:53'),
	(7, 13, 45.00, 0.00, 45.00, 1, '2020-03-22 00:19:02'),
	(8, 13, 45.00, 45.00, 0.00, 0, '2020-03-22 00:19:17'),
	(9, 14, 24.00, 12.00, 12.00, 2, '2020-03-22 00:22:14'),
	(10, 15, 13.00, 0.00, 13.00, 1, '2020-03-22 00:24:33'),
	(11, 16, 13.00, 6.00, 6.00, 2, '2020-03-22 00:26:43'),
	(12, 17, 11.00, 11.00, 0.00, 0, '2020-03-22 00:29:08'),
	(13, 18, 22.00, 22.00, 0.00, 0, '2020-03-22 00:41:18'),
	(14, 19, 26.00, 13.00, 13.00, 2, '2020-03-22 00:42:45'),
	(15, 32, 46.00, 23.00, 23.00, 2, '2020-04-09 21:56:47'),
	(16, 34, 0.00, 0.00, 0.00, 0, '2020-04-09 22:14:14'),
	(17, 36, 0.00, 0.00, 0.00, 1, '2020-04-09 22:15:18'),
	(18, 33, 22.00, 22.00, 0.00, 0, '2020-04-14 23:35:54'),
	(19, 38, 22.00, 0.00, 22.00, 1, '2020-04-14 23:39:30'),
	(20, 35, 26.00, 0.00, 26.00, 1, '2020-04-14 23:41:29'),
	(21, 37, 33.00, 0.00, 33.00, 1, '2020-04-14 23:42:06'),
	(22, 40, 26.00, 0.00, 26.00, 1, '2020-04-14 23:54:13'),
	(23, 39, 13.00, 13.00, 0.00, 0, '2020-04-14 23:54:20'),
	(24, 41, 122.00, 61.00, 61.00, 2, '2020-04-14 23:57:13'),
	(26, 42, 0.00, 233.25, 32.54, 0, '2020-04-20 22:47:05'),
	(27, 42, 0.00, 45.00, 261.75, 0, '2020-04-20 22:55:17'),
	(28, 43, 10.25, 0.00, 0.00, 0, '2020-04-22 19:30:34'),
	(29, 44, 30.75, 30.75, 0.00, 0, '2020-04-22 19:39:49'),
	(30, 45, 30.00, 0.00, 30.00, 0, '2020-04-22 19:40:08'),
	(31, 46, 162.00, 81.00, 81.00, 0, '2020-04-22 19:48:04'),
	(32, 49, 10.25, 10.25, 0.00, 0, '2020-04-22 22:49:41');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.product
CREATE TABLE IF NOT EXISTS `product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(64) DEFAULT NULL,
  `Type` tinyint(4) DEFAULT NULL COMMENT '0 => Yiyecek, 1 => İçecek',
  `Purchase` decimal(10,2) DEFAULT NULL,
  `Sale` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- odev_db.product: ~7 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`ID`, `Title`, `Type`, `Purchase`, `Sale`) VALUES
	(1, 'Meyve Tabağı', 0, 10.00, 14.00),
	(2, 'Meyve Suyu', 1, 8.00, 11.00),
	(3, 'Kahve', 0, 0.25, 2.00),
	(5, 'Test Ürünü 2', 1, 20.00, 30.00),
	(7, 'Nar', 0, 15.00, 25.00),
	(8, 'Başka bir Kahve', 1, 0.50, 10.25),
	(21, 'Su', 0, 1.50, 2.00);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.reservation
CREATE TABLE IF NOT EXISTS `reservation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerName` varchar(32) DEFAULT NULL,
  `ReservationDate` date DEFAULT NULL,
  `ReservationTime` varchar(32) DEFAULT NULL,
  `IsComing` tinyint(4) DEFAULT 1 COMMENT '0 => Geldi, 1 => Gelmedi',
  `CustomerNote` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- odev_db.reservation: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.safe
CREATE TABLE IF NOT EXISTS `safe` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SafeValue` decimal(18,2) DEFAULT NULL,
  `TotalWage` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- odev_db.safe: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `safe` DISABLE KEYS */;
INSERT INTO `safe` (`ID`, `SafeValue`, `TotalWage`) VALUES
	(1, 1682.25, 0.00);
/*!40000 ALTER TABLE `safe` ENABLE KEYS */;

-- tablo yapısı dökülüyor odev_db.safemovement
CREATE TABLE IF NOT EXISTS `safemovement` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Price` decimal(12,2) DEFAULT NULL,
  `PriceDesc` varchar(255) DEFAULT NULL,
  `RegisterDate` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

-- odev_db.safemovement: ~31 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `safemovement` DISABLE KEYS */;
INSERT INTO `safemovement` (`ID`, `Price`, `PriceDesc`, `RegisterDate`) VALUES
	(1, 223.00, '2 Numaralı Masanın ödemesi', '2020-02-20 00:00:00'),
	(2, 509.00, '1 Numaralı Masanın ödemesi', '2020-02-05 00:00:00'),
	(3, 61.00, '2 Numaralı Masanın ödemesi', '2020-03-21 23:29:25'),
	(4, 37.00, '4 Numaralı Masanın ödemesi', '2020-03-21 23:29:36'),
	(5, 11.00, '2 Numaralı Masanın ödemesi', '2020-03-21 23:47:32'),
	(6, 45.00, '5 Numaralı Masanın ödemesi', '2020-03-22 00:18:53'),
	(7, 45.00, '4 Numaralı Masanın ödemesi', '2020-03-22 00:19:02'),
	(8, 45.00, '4 Numaralı Masanın ödemesi', '2020-03-22 00:19:17'),
	(9, 24.00, '1 Numaralı Masanın ödemesi', '2020-03-22 00:22:14'),
	(10, 13.00, '4 Numaralı Masanın ödemesi', '2020-03-22 00:24:34'),
	(11, 13.00, '5 Numaralı Masanın ödemesi', '2020-03-22 00:26:43'),
	(12, 11.00, '4 Numaralı Masanın ödemesi', '2020-03-22 00:29:08'),
	(13, 22.00, '4 Numaralı Masanın ödemesi', '2020-03-22 00:41:18'),
	(14, 26.00, '1 Numaralı Masanın ödemesi', '2020-03-22 00:42:46'),
	(15, 46.00, '4 Numaralı Masanın ödemesi', '2020-04-09 21:56:47'),
	(16, 0.00, '2 Numaralı Masanın ödemesi', '2020-04-09 22:14:14'),
	(17, 0.00, '2 Numaralı Masanın ödemesi', '2020-04-09 22:15:18'),
	(18, 22.00, '5 Numaralı Masanın ödemesi', '2020-04-14 23:35:55'),
	(19, 22.00, '2 Numaralı Masanın ödemesi', '2020-04-14 23:39:30'),
	(20, 26.00, '4 Numaralı Masanın ödemesi', '2020-04-14 23:41:30'),
	(21, 33.00, '2 Numaralı Masanın ödemesi', '2020-04-14 23:42:06'),
	(22, 26.00, '1 Numaralı Masanın ödemesi', '2020-04-14 23:54:14'),
	(23, 13.00, '2 Numaralı Masanın ödemesi', '2020-04-14 23:54:20'),
	(24, 122.00, '4 Numaralı Masanın ödemesi', '2020-04-14 23:57:13'),
	(25, 44.00, '7 Numaralı Masanın ödemesi', '2020-04-15 00:01:12'),
	(26, 0.00, '7 Numaralı Masanın ödemesi', '2020-04-20 22:47:05'),
	(27, 0.00, '7 Numaralı Masanın ödemesi', '2020-04-20 22:55:17'),
	(28, 10.25, '7 Numaralı Masanın ödemesi', '2020-04-22 19:30:34'),
	(29, 30.75, '2 Numaralı Masanın ödemesi', '2020-04-22 19:39:49'),
	(30, 30.00, '1 Numaralı Masanın ödemesi', '2020-04-22 19:40:09'),
	(31, 162.00, '8 Numaralı Masanın ödemesi', '2020-04-22 19:48:04'),
	(32, 10.25, '1 Numaralı Masanın ödemesi', '2020-04-22 22:49:41');
/*!40000 ALTER TABLE `safemovement` ENABLE KEYS */;

-- görünüm yapısı dökülüyor odev_db.vw_additionlist
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_additionlist` (
	`AdditionID` INT(11) NOT NULL,
	`TotalPrice` DECIMAL(12,2) NULL,
	`PayStatus` VARCHAR(15) NULL COLLATE 'utf8mb4_general_ci',
	`PayStatusValue` TINYINT(4) NULL,
	`Status` TINYINT(4) NULL,
	`DeskID` INT(11) NULL,
	`DeskPosition` VARCHAR(8) NULL COLLATE 'utf8mb4_general_ci',
	`DeskTitle` VARCHAR(32) NULL COLLATE 'utf8mb4_general_ci',
	`DeskIsOpen` TINYINT(4) NULL,
	`AdditionMovementCount` BIGINT(21) NOT NULL
) ENGINE=MyISAM;

-- görünüm yapısı dökülüyor odev_db.vw_additionmovementlist
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_additionmovementlist` (
	`AdditionMovementID` INT(11) NOT NULL,
	`AdditionID` INT(11) NULL,
	`MovementTitle` VARCHAR(64) NULL COLLATE 'utf8mb4_general_ci',
	`Price` DECIMAL(12,2) NULL,
	`ProductCount` TINYINT(3) UNSIGNED NULL,
	`DescText` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`ProductID` INT(11) NULL,
	`Title` VARCHAR(64) NULL COLLATE 'utf8mb4_general_ci',
	`pType` VARCHAR(7) NULL COLLATE 'utf8mb4_unicode_ci',
	`Sale` DECIMAL(10,2) NULL
) ENGINE=MyISAM;

-- görünüm yapısı dökülüyor odev_db.vw_desklist
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_desklist` (
	`ID` INT(11) NOT NULL,
	`DeskTitle` VARCHAR(32) NULL COLLATE 'utf8mb4_general_ci',
	`DeskPosition` VARCHAR(8) NULL COLLATE 'utf8mb4_unicode_ci',
	`DeskIsOpen` VARCHAR(6) NULL COLLATE 'utf8mb4_unicode_ci',
	`DeskPositionValue` TINYINT(4) NULL,
	`DeskIsOpenValue` TINYINT(4) NULL
) ENGINE=MyISAM;

-- görünüm yapısı dökülüyor odev_db.vw_desk_history_items
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_desk_history_items` (
	`AdditionID` INT(11) NOT NULL,
	`AdditionMovementID` INT(11) NULL,
	`MovementTitle` VARCHAR(64) NULL COLLATE 'utf8mb4_general_ci',
	`AdditionMovementPrice` DECIMAL(12,2) NULL,
	`ProductCount` TINYINT(3) UNSIGNED NULL,
	`SalesPrice` DECIMAL(10,2) NULL,
	`Title` VARCHAR(64) NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- görünüm yapısı dökülüyor odev_db.vw_employeelist
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_employeelist` (
	`ID` INT(11) NOT NULL,
	`Username` VARCHAR(32) NULL COLLATE 'utf8mb4_general_ci',
	`Password` VARCHAR(32) NULL COLLATE 'utf8mb4_general_ci',
	`DepartmentID` TINYINT(4) NULL,
	`Wage` DECIMAL(12,2) NULL,
	`DepartmentValue` VARCHAR(32) NULL COLLATE 'utf8mb4_general_ci',
	`DepartmentDesc` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- görünüm yapısı dökülüyor odev_db.vw_menuproductlist
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_menuproductlist` (
	`Count` TINYINT(4) NULL,
	`Sale` DECIMAL(10,2) NULL,
	`Title` VARCHAR(64) NULL COLLATE 'utf8mb4_general_ci',
	`ProductType` VARCHAR(7) NULL COLLATE 'utf8mb4_unicode_ci',
	`ProductID` INT(11) NOT NULL,
	`MenuID` INT(11) NULL,
	`MenuProductID` INT(11) NOT NULL
) ENGINE=MyISAM;

-- görünüm yapısı dökülüyor odev_db.vw_menu_details
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_menu_details` (
	`MenuID` INT(11) NOT NULL,
	`ProductCount` BIGINT(21) NOT NULL,
	`MenuPrice` DECIMAL(12,2) NULL,
	`MenuTotalPrice` DECIMAL(12,2) NULL,
	`MenuIsActive` VARCHAR(7) NULL COLLATE 'utf8mb4_unicode_ci'
) ENGINE=MyISAM;

-- görünüm yapısı dökülüyor odev_db.vw_productlist
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `vw_productlist` (
	`ProductID` INT(11) NOT NULL,
	`Title` VARCHAR(64) NULL COLLATE 'utf8mb4_general_ci',
	`Type` TINYINT(4) NULL COMMENT '0 => Yiyecek, 1 => İçecek',
	`ProductType` VARCHAR(7) NULL COLLATE 'utf8mb4_general_ci',
	`Purchase` DECIMAL(10,2) NULL,
	`Sale` DECIMAL(10,2) NULL,
	`PriceDiff` DECIMAL(11,2) NULL
) ENGINE=MyISAM;

-- yöntem yapısı dökülüyor odev_db.prcAdditionAddProduct
DELIMITER //
CREATE  PROCEDURE `prcAdditionAddProduct`(
	IN `productID` INT,
	IN `productCount` INT,
	IN `additionID` INT

)
BEGIN
	DECLARE additionMovementID INT DEFAULT 0;
	DECLARE ProductTitle VARCHAR(64) DEFAULT "";
	DECLARE DescText VARCHAR(128) DEFAULT "";
	DECLARE ProductSalePrice DECIMAL(12,2);
	DECLARE ProductSaleTotalPrice DECIMAL (12,2);
	
	SELECT Title, Sale, Sale * productCount INTO ProductTitle, ProductSalePrice, ProductSaleTotalPrice FROM product WHERE ID = productID;
	SELECT CONCAT(ProductTitle, " Adlı Üründen ", productCount, " Adet Sipariş Verilmiştir; Toplam : ", (ProductSalePrice * productCount), " ₺ Tutmuştur") INTO DescText;

	
	INSERT INTO additionmovement(AdditionID, MovementTitle, Price) VALUES (additionID, ProductTitle, ProductSaleTotalPrice);
	SELECT LAST_INSERT_ID() INTO additionMovementID;
	INSERT INTO additionmovementproduct(ProductID, ProductCount, DescText, AdditionMovementID) VALUES (productID, productCount, DescText, additionMovementID);
	SELECT additionMovementID as AdditionMovementID;
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcAdditionPayment
DELIMITER //
CREATE  PROCEDURE `prcAdditionPayment`(
	IN `additionID` INT,
	IN `paymentType` TINYINT,
	IN `totalPrice` DECIMAL(12,2),
	IN `cashPrice` DECIMAL(12,2),
	IN `cartPrice` DECIMAL(12,2),
	IN `deskID` INT


)
BEGIN
	INSERT INTO payment (AdditionID, TotalPrice, CartPrice, CashPrice, PaymentType) VALUES (additionID, totalPrice, cartPrice, cashPrice, paymentType);
	UPDATE desk SET DeskIsOpen = 0 WHERE ID = deskID;
	UPDATE addition SET PayStatus = 1, Status = 0 WHERE ID = additionID;
	INSERT INTO safemovement (Price, PriceDesc) VALUES (totalPrice, CONCAT(deskID, " Numaralı Masanın ödemesi"));
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcCreateAddition
DELIMITER //
CREATE  PROCEDURE `prcCreateAddition`(IN deskID INT)
BEGIN
  UPDATE desk SET DeskIsOpen = 1 WHERE ID = deskID;
  INSERT INTO addition(DeskID) VALUES (deskID);
  SELECT LAST_INSERT_ID() AS 'AdditionID';
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcCreateMenu
DELIMITER //
CREATE  PROCEDURE `prcCreateMenu`(
	IN `_Title` VARCHAR(50),
	IN `_Price` DECIMAL(12,2),
	IN `_TotalPrice` DECIMAL(12,2)
)
BEGIN
	INSERT INTO menu (MenuTitle, MenuPrice, MenuTotalPrice) VALUES (_Title, _Price, _TotalPrice);
	SELECT LAST_INSERT_ID() AS 'MenuID';
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcCreateNewDesk
DELIMITER //
CREATE  PROCEDURE `prcCreateNewDesk`(
	IN `deskTitle` VARCHAR(50),
	IN `deskPosition` TINYINT
)
BEGIN
	INSERT INTO desk(DeskTitle, DeskPosition) VALUES (deskTitle, deskPosition);
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcEditDesk
DELIMITER //
CREATE  PROCEDURE `prcEditDesk`(
	IN `deskID` INT,
	IN `deskTitle` VARCHAR(50),
	IN `deskPosition` TINYINT
)
BEGIN
	UPDATE desk SET DeskTitle = deskTitle, DeskPosition = deskPosition WHERE ID = deskID;
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcEmployeeAdd
DELIMITER //
CREATE  PROCEDURE `prcEmployeeAdd`()
BEGIN
	INSERT INTO employee (Username, Password, Wage, DepartmentID) VALUES (uname, pwd, wage, depID);
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcInsertMenuItem
DELIMITER //
CREATE  PROCEDURE `prcInsertMenuItem`(
	IN `menuID` INT,
	IN `productID` INT,
	IN `count` INT
)
BEGIN
	INSERT INTO menuproduct(MenuID, ProductID, Count) VALUES (menuID, productID, count);
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcInsertProduct
DELIMITER //
CREATE  PROCEDURE `prcInsertProduct`(IN `product_title` VARCHAR(255), IN `product_purchase_price` DECIMAL, IN `product_sale_price` DECIMAL, IN `product_type` TINYINT)
    NO SQL
BEGIN
	INSERT INTO product(Title, Type, Purchase, Sale) values (product_title, product_type, product_purchase_price, product_sale_price);

	SELECT LAST_INSERT_ID() AS InsertedProductID;
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcLogin
DELIMITER //
CREATE  PROCEDURE `prcLogin`(
	IN `uname` VARCHAR(50),
	IN `pwd` VARCHAR(50)
)
BEGIN
	SELECT * FROM vw_employeelist WHERE Username = uname AND Password = pwd;
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcMenuRemoveItem
DELIMITER //
CREATE  PROCEDURE `prcMenuRemoveItem`(
	IN `menuID` INT,
	IN `menuProductID` INT,
	IN `productPrice` DECIMAL(12,2)
)
BEGIN
	UPDATE menu m SET m.MenuTotalPrice = m.MenuTotalPrice - productPrice WHERE m.ID = menuID;
	DELETE FROM menuproduct WHERE ID = menuProductID;
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcRemoveProductItem
DELIMITER //
CREATE  PROCEDURE `prcRemoveProductItem`(IN productID INT)
BEGIN
  DELETE FROM product WHERE ID = productID;
  DELETE FROM menuproduct WHERE ProductID = productID;
END//
DELIMITER ;

-- yöntem yapısı dökülüyor odev_db.prcUpdateProductItem
DELIMITER //
CREATE  PROCEDURE `prcUpdateProductItem`(IN pID INT, IN title VARCHAR(255), IN type TINYINT, IN purchase DECIMAL(12,2), IN sale DECIMAL(12,2))
BEGIN
  UPDATE product SET Title = title, Type = type, Purchase = purchase, Sale = sale WHERE ID = pID;
  /*Menü Fiyatları Güncellenebilir*/ 
END//
DELIMITER ;

-- tetikleyici yapısı dökülüyor odev_db.after_additionmovement_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `after_additionmovement_delete` AFTER DELETE ON `additionmovement` FOR EACH ROW BEGIN
	DELETE FROM additionmovementproduct 
	WHERE additionmovementproduct.AdditionMovementID = OLD.ID;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- tetikleyici yapısı dökülüyor odev_db.after_additionmovement_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `after_additionmovement_insert` AFTER INSERT ON `additionmovement` FOR EACH ROW IF (SELECT TotalPrice FROM addition WHERE ID = new.AdditionID) > 0 THEN
UPDATE addition SET TotalPrice = (TotalPrice + new.Price) WHERE ID = new.AdditionID;
else
UPDATE addition SET TotalPrice = new.Price WHERE ID = new.AdditionID;
END IF//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- tetikleyici yapısı dökülüyor odev_db.after_addition_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `after_addition_delete` AFTER DELETE ON `addition` FOR EACH ROW BEGIN
	DELETE FROM additionmovement WHERE AdditionID = OLD.ID;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- tetikleyici yapısı dökülüyor odev_db.after_product_update
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `after_product_update` AFTER UPDATE ON `product` FOR EACH ROW BEGIN
  DECLARE MPID int;
  SELECT MenuID INTO MPID FROM menuproduct WHERE ProductID = old.ID;
  IF MPID > 0 THEN
    UPDATE menu SET MenuTotalPrice = MenuTotalPrice + (old.Sale - old.Purchase) WHERE ID = MPID;
  END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- tetikleyici yapısı dökülüyor odev_db.after_safemovement_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `after_safemovement_insert` AFTER INSERT ON `safemovement` FOR EACH ROW UPDATE safe SET SafeValue = (SafeValue + new.Price)//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- görünüm yapısı dökülüyor odev_db.vw_additionlist
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_additionlist`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_additionlist` AS SELECT 
a.ID AS AdditionID,
a.TotalPrice, 
(
	CASE
		WHEN a.PayStatus = 0 THEN 'Ödeme Yapılmadı'
		WHEN a.PayStatus = 1 THEN 'Ödeme Yapıldı'
	END
) AS PayStatus,
a.PayStatus As PayStatusValue,
a.Status,
d.ID AS DeskID, 
(
	CASE 
		WHEN d.DeskPosition = 0 THEN 'İçeride'
		WHEN d.DeskPosition = 1 THEN 'Dışarıda' 
		WHEN d.DeskPosition = 2 THEN 'Bahçede'
	END
) AS DeskPosition,
d.DeskTitle,
d.DeskIsOpen,
COUNT(am.ID) AS AdditionMovementCount
FROM addition a
LEFT JOIN additionmovement am ON am.AdditionID = a.ID
LEFT JOIN desk d ON d.ID = a.DeskID
GROUP BY a.ID ;

-- görünüm yapısı dökülüyor odev_db.vw_additionmovementlist
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_additionmovementlist`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_additionmovementlist` AS SELECT 
am.ID AS AdditionMovementID,
am.AdditionID,
am.MovementTitle,
am.Price,
amp.ProductCount,
amp.DescText,
p.ID AS ProductID,
p.Title,
(
	CASE 
		WHEN p.Type = 0 THEN "Yiyecek"
		WHEN p.Type = 1 THEN "İçecek"
	END
) AS pType,
p.Sale
FROM additionmovement am
LEFT JOIN additionmovementproduct amp ON am.ID = amp.AdditionMovementID
LEFT JOIN product p ON p.ID = amp.ProductID ;

-- görünüm yapısı dökülüyor odev_db.vw_desklist
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_desklist`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_desklist` AS SELECT ID, DeskTitle, (
	CASE
		WHEN DeskPosition = 0 THEN "İçeride"
		WHEN DeskPosition = 1 THEN "Dışarıda"
		WHEN DeskPosition = 2 THEN "Bahçede"
	END
) AS DeskPosition, 
(
	CASE
		WHEN DeskIsOpen = 0 THEN "Kapalı"
		WHEN DeskIsOpen = 1 THEN "Açık"
	END
) AS DeskIsOpen,
DeskPosition as DeskPositionValue,
DeskIsOpen as DeskIsOpenValue
FROM desk ;

-- görünüm yapısı dökülüyor odev_db.vw_desk_history_items
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_desk_history_items`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_desk_history_items` AS SELECT 
a.ID as AdditionID,
am.ID as AdditionMovementID,
am.MovementTitle,
am.Price AS AdditionMovementPrice,
amp.ProductCount,
p.Sale As SalesPrice,
p.Title
FROM addition a
LEFT JOIN additionmovement am ON am.AdditionID = a.ID
LEFT JOIN additionmovementproduct amp ON amp.AdditionMovementID = am.ID
LEFT JOIN product p on p.ID = amp.ProductID ;

-- görünüm yapısı dökülüyor odev_db.vw_employeelist
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_employeelist`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_employeelist` AS SELECT 
e.*,
dp.DepartmentValue,
dp.DepartmentDesc
FROM employee e 
LEFT JOIN department dp ON dp.ID = e.DepartmentID ;

-- görünüm yapısı dökülüyor odev_db.vw_menuproductlist
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_menuproductlist`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_menuproductlist` AS SELECT mp.Count, p.Sale, p.Title, 
(CASE 
	WHEN p.Type = 0 THEN "Yiyecek"
	WHEN p.Type = 1 THEN "İçecek"
	END) AS ProductType,
mp.ProductID,
mp.MenuID,
mp.ID as MenuProductID
FROM menuproduct mp
LEFT JOIN menu m ON m.ID = mp.MenuID
LEFT JOIN product p ON p.ID = mp.ProductID ;

-- görünüm yapısı dökülüyor odev_db.vw_menu_details
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_menu_details`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_menu_details` AS SELECT
    `m`.`ID` AS `MenuID`,
    COUNT(`mp`.`Count`) AS `ProductCount`,
    `m`.`MenuPrice` AS `MenuPrice`,
    `m`.`MenuTotalPrice` AS `MenuTotalPrice`,
    CASE WHEN `m`.`IsActive` = 1 THEN 'Aktif' WHEN `m`.`IsActive` = 0 THEN 'Deaktif'
END AS `MenuIsActive`
FROM
    (
        `menu` `m`
    LEFT JOIN `menuproduct` `mp`
    ON
        (`mp`.`MenuID` = `m`.`ID`)
    )
GROUP BY
    mp.MenuID ;

-- görünüm yapısı dökülüyor odev_db.vw_productlist
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `vw_productlist`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `vw_productlist` AS SELECT
  product.ID AS ProductID,
  product.Title,
  product.Type,
  (
  	CASE
  		WHEN product.Type = 0 THEN 'Yiyecek'
		WHEN product.Type = 1 THEN 'İçecek'	
	END
  ) AS ProductType,
  product.Purchase,
  product.Sale,
  product.Sale - product.Purchase AS PriceDiff
FROM product ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
